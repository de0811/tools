#!/usr/bin/python3
# coding=utf8
#-*-coding:utf-8-*-

import sys
import os
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import threading
import time
import android.apkinstaller
import android.apkrunner
from lib.option import option
# from lib.androidinfo import *

class LogcatBookmark() :
    u"""
    class LogcatBookmark
    호환성 테스트때 logcat 분석이 정상적으로 되지 않는 부분이 있어서 재분석을 위해 개발
    """
    testDir = str()
    apk_name = str()
    def __init__(self) :
        self.testDir = str()
        self.apk_name = str()

    def set_test_count(self, args) :
        u"""option : -c"""
        if len(args) != 1 : 
            print ("Args Error")
            print ("Args : Test count 1")
            sys.exit()
        val = args[0]
        try :
            self.testCount = int(val)
        except ValueError :
            print ("Args Error")
            print ("Args : Test Count is Number !! ")
            sys.exit()
    
    def set_test_directory(self, args) :
        u"""option : -d"""
        if len(args) != 1 :
            print ("Args Error")
            print ("Args : WorkDirectory Error")
            sys.exit()
        self.testDir = args[0]
    
    def set_apk_name(self, args):
        u"""option : -a"""
        if len(args) != 1 :
            print ("Args Error")
            print ("Args : WorkDirectory Error")
            sys.exit()
        self.apk_name = args[0]

    def help(self, args) :
        u""" option : -h """
        hel = u'''LogcatBookmark.py [command] [target]
        기기 별 호환성 테스트를 진행합니다.
        [command]
        -h : 설명을 나타냅니다
        -c : APK 별 검증 횟수를 설정합니다.
        -d : 분석할 APK가 저장된 디렉토리를 지정합니다.
        '''
        print (hel)
        sys.exit()
    
    def __logcat_line_parssing(self, log_data):
        saveList = list()
        log_split = log_data.split('\n')
        for line in log_split :
            # print (line)
            if line.find("V/WindowManager") != -1 or\
                line.find(".png") != -1 or\
                    line.find("V WindowManager:") != -1 or\
                        line.find("remove failed: ENOENT (No such file or directory)") != -1 :
                continue
            if line.find("Process: " + self.apk_name + ", PID: ") != -1 :
                saveList.append(line)
                # break
            if ( (line.find("Exception") != -1 or \
                line.find("Error") != -1 or \
                    line.find("failed") != -1 or \
                        line.find("\tat ") != -1 or \
                            line.find(" at ") != -1\
                            ) and \
                                line.find(self.apk_name) != -1) :
                # jar이 없다고 뜨는 문제는 무시
                if line.find(".jar ") != -1 :
                    continue
                saveList.append(line)
                # break
        return saveList

    def get_dir_list(self, dir):
        fList = list()
        for root, dirs, files in os.walk(dir):
            for fname in files:
                fullpath = os.path.join(root, fname)
                # fList.append(fullpath[len(dir):])
                fList.append(fullpath)
        return fList
   
    def __test_run(self) :
        # apk 목록 뽑아오기
        fileList = self.get_dir_list(self.testDir)
        logcatList = list()
        for fileName in fileList :
            if fileName.find('logcat') != -1 :
                print (fileName)
                logcatList.append(fileName)

        for logcat in logcatList :
            parssingList = list()
            with open(logcat, "rt", encoding='UTF8') as f:
                readed = f.read()
                parssingList = self.__logcat_line_parssing(readed)
                # print ('*' * 30)
                # print (parssingList)
                # print ('*' * 30)
            pass
            savePath = os.path.dirname(logcat) + os.sep + 'bookmark.txt'
            print ('SAVE PATH ::: ' + savePath)
            print (parssingList)
            if len(parssingList) > 0 :
                with open(savePath, 'wt', encoding='UTF8') as f:
                    f.write(logcat)
                    f.write('\n')
                    for parssing in parssingList :
                        f.write(parssing)
                        f.write('\n')
                    f.write('\n')
    

    def run(self, args) :
        u"""전체 실행 부분"""
        self.__test_run()
        # for tick in self.testCount :
            # pass

if __name__ == "__main__":

    #argvs = ["-h",]
    argvs = list()
    opt = option()
    logcatBookmark = LogcatBookmark()
    #def addOpt(self, opt, argCount, bVarArg, func):
    opt.addOpt(opt="-h", argCount=0, bVarArg=True, bHelp=True, func=logcatBookmark.help)
    opt.addOpt(opt="-d", argCount=1, bVarArg=True, bHelp=False, func=logcatBookmark.set_test_directory)
    opt.addOpt(opt="-a", argCount=1, bVarArg=True, bHelp=False, func=logcatBookmark.set_apk_name)
    opt.addOpt(opt="default", argCount=0, bVarArg=True, bHelp=False, func=logcatBookmark.run)
    test = ['-d' , 'C:\\temp\\compatibility_50_dx\\']
    test = ['-a', 'com.ibk.farm', '-d' , 'C:\\temp\\compatibility_20_dx\\com.ibk.farm']
    opt.parsing(test)
    # opt.parsing(argvs)
    opt.run()