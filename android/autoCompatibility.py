#!/usr/bin/python3
# coding=utf8
#-*-coding:utf-8-*-

import sys
import os
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

import threading
import time
import android.apkinstaller
import android.apkrunner
from lib.option import option
# from lib.androidinfo import *

class CompatibilityManager() :
    u"""
    class CompatibilityManager
    호환성 테스트를 중심에서 컨트롤하는 컨트롤러
    """
    installerList = list()
    # apk_installer = apkinstaller.ApkInstaller()
    # apk_runner = android.apkrunner.ApkRunner()
    testCount = 0
    testDir = str()
    def __init__(self) :
        self.installerList = list()
        # self.apk_installer = apkinstaller.ApkInstaller()
        # self.apk_runner = android.apkrunner.ApkRunner()
        self.testCount = 0
        self.testDir = str()

    def set_test_count(self, args) :
        u"""option : -c"""
        if len(args) != 1 : 
            print ("Args Error")
            print ("Args : Test count 1")
            sys.exit()
        val = args[0]
        try :
            self.testCount = int(val)
        except ValueError :
            print ("Args Error")
            print ("Args : Test Count is Number !! ")
            sys.exit()
    
    def set_test_directory(self, args) :
        u"""option : -d"""
        if len(args) != 1 :
            print ("Args Error")
            print ("Args : WorkDirectory Error")
            sys.exit()
        self.testDir = args[0]

    def help(self, args) :
        u""" option : -h """
        hel = u'''autoApkDevicesCompatibility.py [command] [target]
        기기 별 호환성 테스트를 진행합니다.
        [command]
        -h : 설명을 나타냅니다
        -c : APK 별 검증 횟수를 설정합니다.
        -d : 분석할 APK가 저장된 디렉토리를 지정합니다.
        '''
        print (hel)
        sys.exit()

    def get_dir_list(self, dir):
        fList = list()
        for root, dirs, files in os.walk(dir):
            for fname in files:
                fullpath = os.path.join(root, fname)
                # fList.append(fullpath[len(dir):])
                fList.append(fullpath)
        return fList
   
    def __test_run(self) :
        # apk 목록 뽑아오기
        print ("testCount :: " + str(self.testCount))
        print (self.testDir)
        fileList = self.get_dir_list(self.testDir)
        print (fileList)
        for filePathName in fileList :
            for count in range(self.testCount) :
                print ("Current Test Count [" + str(count) + "]")
                print (str(count) + "  " + filePathName)
                apk_installer = android.apkinstaller.ApkInstaller()
                # apk_installer.device_finder.select_sdk(['29'])
                apk_installer.apk_install(filePathName)
                # runner 진행
                apk_runner = android.apkrunner.ApkRunner()
                # apk_runner.device_finder.select_sdk(['29'])
                apk_runner.set_apk([filePathName])
                apk_runner.set_time_limit(['60'])
                apk_runner.set_time_wait(['5'])
                apk_runner.apk_running()
                # 
                apk_installer.apk_uninstall(filePathName)
        pass

    def run(self, args) :
        u"""전체 실행 부분"""
        self.__test_run()
        # for tick in self.testCount :
            # pass

if __name__ == "__main__":

    #argvs = ["-h",]
    argvs = list()
    opt = option()
    compatibilityManager = CompatibilityManager()
    #def addOpt(self, opt, argCount, bVarArg, func):
    opt.addOpt(opt="-h", argCount=0, bVarArg=True, bHelp=True, func=compatibilityManager.help)
    opt.addOpt(opt="-c", argCount=1, bVarArg=True, bHelp=False, func=compatibilityManager.set_test_count)
    opt.addOpt(opt="-d", argCount=1, bVarArg=True, bHelp=False, func=compatibilityManager.set_test_directory)
    opt.addOpt(opt="default", argCount=0, bVarArg=True, bHelp=False, func=compatibilityManager.run)
    test = ['-c', '10', '-d' , 'C:\\temp\\check_app']
    opt.parsing(test)
    # opt.parsing(argvs)
    opt.run()