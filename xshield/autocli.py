#!/usr/bin/python3
#-*-coding:utf-8-*-

import os
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
import random
import time
from lib import option
from lib import runprocess
import config
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import ThreadPoolExecutor

current_path = os.path.dirname(os.path.realpath(__file__))
'''
정상 테스트
Param 테스트
 - Param 미 적용 테스트
 - Param 오타 테스트
value 테스트
'''


class eCLI_TYPE :
    TYPE_FX = "fx"
    TYPE_TFX = "tfx"
    TYPE_GX = "gx"
    TYPE_TGX = "tgx"
    TYPE_DUO = "duo"
    TYPE_TDUO = "tduo"
    TYPE_ALT = "alt"
    TYPE_TALT = "talt"
    TYPE_INSTALL = "install"

class ePARAM_TYPE :
    TYPE_USERID = 0                                         #사이트 별 아이디
    TYPE_PASSWD = 1                                         #사이트 별 비밀번호
    TYPE_ENGINE_VER = 2                                     #엔진 버전
    TYPE_PACK_OPT = 3                                       #패킹 옵션
    TYPE_DETECT_OPT = 4                                     #디텍트 옵션
    TYPE_ORGN_PATH = 5                                      #apk 경로
    TYPE_SAVE_PATH = 6                                      #보안된 apk 저장할 위치
    TYPE_CONFIRM = 7                                        #Yes or No 물어보는 것 (테스트 생략)
    TYPE_OVER_WRITE = 8                                     #덮어 씌울지 아니면 (1) 처럼 중복되지 않도록 파일 저장할지 선택
    TYPE_ENC_PATH = 9                                       #암호화 설정 파일
    TYPE_SAVE_ORGN_APK = 10                                 #APK 서버에 저장시킬지 말지 저장
    TYPE_SITE = 11                                          #사이트 주소
    TYPE_NORMAL_END = TYPE_SITE + 1

    TYPE_DPG_MESSAGE = 12
    TYPE_DPG_RNOTE = 13
    TYPE_DPG_END = TYPE_DPG_RNOTE + 1

    TYPE_KEYSTORE = 14
    TYPE_STOREPASS = 15
    TYPE_ALIAS = 16
    TYPE_KEYPASS = 17
    TYPE_END = TYPE_KEYPASS + 1

class AutoCli :
    def __init__(self):
        self.cli_type = eCLI_TYPE.TYPE_FX
        current_path = os.path.dirname(os.path.realpath(__file__))
        self.log_list = list()
        #self.p = Properties()

        #변하지 않는 고정적인 내용 저장
        self.value = dict()
        self.value.setdefault(ePARAM_TYPE.TYPE_USERID, ["QaAdmin", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_PASSWD, ["4pS]\\Kd~", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_ENGINE_VER, ["5.0.0", "6.0.0", "5.0.0.1", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_PACK_OPT, [Properties().random_pack_option(), "z", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_DETECT_OPT, [Properties().random_detect_option(), "O", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_ORGN_PATH, [current_path + os.sep + "apk" + os.sep + "1.apk", current_path + os.sep + "apk" + os.sep + "2.apk", current_path + os.sep + "apk" + os.sep + "3.apk", current_path + os.sep + "apk" + os.sep + "4.apk", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_SAVE_PATH, [current_path + os.sep + "result",  current_path + os.sep + "asdasd", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_CONFIRM, ["false", "ffff", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_OVER_WRITE, ["true", "false", "ffffff", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_ENC_PATH, [current_path + os.sep + "custom.ini", current_path + os.sep + "aaaaa.ini", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_SAVE_ORGN_APK, ["true", "false", "ffff", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_SITE, ["http://", ""])
       
        now = time.localtime() 
        current_time = "%04d_%02d_%02d_%02d-%02d-%02d" % (now.tm_year, now.tm_mon, now.tm_mday, now.tm_hour, now.tm_min, now.tm_sec)
        self.value.setdefault(ePARAM_TYPE.TYPE_DPG_MESSAGE, ["Type_Message_%s_練習です" % current_time, ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_DPG_RNOTE, ["Type_Note_%s_練習です" % current_time, ""])

        self.value.setdefault(ePARAM_TYPE.TYPE_KEYSTORE, [config.sigkey_path, current_path + os.sep + "xxxxxx.jsk", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_STOREPASS, ["123456", "naramasami", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_ALIAS, ["testsig", "naramasami", ""])
        self.value.setdefault(ePARAM_TYPE.TYPE_KEYPASS, ["123456", "naramasami", ""])
 

        self.param = dict()
        self.param.setdefault(ePARAM_TYPE.TYPE_USERID, ["userId", "USER", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_PASSWD, ["passwd", "PWD", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_ENGINE_VER, ["engineVer", "VER", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_PACK_OPT, ["packOpt", "PACK", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_DETECT_OPT, ["detectOpt", "DETECT", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_ORGN_PATH, ["orgnPath", "ORG", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_SAVE_PATH, ["savePath", "SAPE", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_CONFIRM, ["confirm", "CON", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_OVER_WRITE, ["overWrite", "OVER", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_ENC_PATH, ["encPath", "ENC", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_SAVE_ORGN_APK, ["saveOrgnApk", "SAOR", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_SITE, ["site", "SSTT", ""])

        self.param.setdefault(ePARAM_TYPE.TYPE_DPG_MESSAGE, ["dpgMessage", "DPGMSG", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_DPG_RNOTE, ["dpgRNote", "DPGNOTE", ""])

        self.param.setdefault(ePARAM_TYPE.TYPE_KEYSTORE, ["keystore", "KEY", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_STOREPASS, ["storepass", "STORE", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_ALIAS, ["alias", "ALI", ""])
        self.param.setdefault(ePARAM_TYPE.TYPE_KEYPASS, ["keypass", "KEY", ""])
    
    #사이트별 처리
    def __set_fx(self) :
        self.value[ePARAM_TYPE.TYPE_USERID] = ["QaAdmin", "QQQQ", ""]
        self.value[ePARAM_TYPE.TYPE_PASSWD] = ["4pS]\\Kd~", "PPPPP", ""]
        self.value[ePARAM_TYPE.TYPE_SITE] = ["https://stage.fxshield.co.kr", "http", ""]
    def __set_tfx(self) :
        self.value[ePARAM_TYPE.TYPE_USERID] = ["QaAdmin", "QQQQ", ""]
        self.value[ePARAM_TYPE.TYPE_PASSWD] = ["b4;7WYyM", "PPPPP", ""]
        self.value[ePARAM_TYPE.TYPE_SITE] = ["https://test.gxshield.com", "http", ""]
    def __set_gx(self) :
        self.value[ePARAM_TYPE.TYPE_USERID] = ["QaAdmin", "QQQQ", ""]
        self.value[ePARAM_TYPE.TYPE_PASSWD] = ["D6/6nnD!", "PPPPP", ""]
        self.value[ePARAM_TYPE.TYPE_SITE] = ["https://stage.gxshield.com", "http", ""]
    def __set_tgx(self) :
        self.value[ePARAM_TYPE.TYPE_USERID] = ["QaAdmin", "QQQQ", ""]
        self.value[ePARAM_TYPE.TYPE_PASSWD] = ["b4;7WYyM", "PPPPP", ""]
        self.value[ePARAM_TYPE.TYPE_SITE] = ["https://test.gxshield.com", "http", ""]
    def __set_alt(self) :
        self.value[ePARAM_TYPE.TYPE_USERID] = ["QaAdmin", "QQQQ", ""]
        self.value[ePARAM_TYPE.TYPE_PASSWD] = ["E8-6:vz#", "PPPPP", ""]
        self.value[ePARAM_TYPE.TYPE_SITE] = ["https://staging.altplus.dxshield.jp", "http", ""]
    def __set_talt(self) :
        self.value[ePARAM_TYPE.TYPE_USERID] = ["QaAdmin", "QQQQ", ""]
        self.value[ePARAM_TYPE.TYPE_PASSWD] = ["b4;7WYyM", "PPPPP", ""]
        self.value[ePARAM_TYPE.TYPE_SITE] = ["https://test.gxshield.com", "http", ""]
    def __set_duo(self) :
        self.value[ePARAM_TYPE.TYPE_USERID] = ["QaAdmin", "QQQQ", ""]
        self.value[ePARAM_TYPE.TYPE_PASSWD] = ["X?8'Wb`$", "ppppp", ""]
        self.value[ePARAM_TYPE.TYPE_SITE] = ["https://stage.duoshield.co.kr", "http", ""]
    def __set_install(self) :
        self.value[ePARAM_TYPE.TYPE_USERID] = ["syjungTest", "QQQQ", ""]
        self.value[ePARAM_TYPE.TYPE_PASSWD] = ["4pS]\\Kd~", "PPPPP", ""]
        self.value[ePARAM_TYPE.TYPE_SITE] = ["http://13.124.111.210:80", "http", ""]
    
    
    def set_test_type(self, args) :
        self.cli_type = args[0]
        if self.cli_type == eCLI_TYPE.TYPE_FX :
            self.__set_fx()
        elif self.cli_type == eCLI_TYPE.TYPE_TFX :
            self.__set_tfx()
        elif self.cli_type == eCLI_TYPE.TYPE_GX :
            self.__set_gx()
        elif self.cli_type == eCLI_TYPE.TYPE_TGX :
            self.__set_tgx()
        elif self.cli_type == eCLI_TYPE.TYPE_ALT :
            self.__set_alt()
        elif self.cli_type == eCLI_TYPE.TYPE_TALT :
            self.__set_talt()
        elif self.cli_type == eCLI_TYPE.TYPE_DUO :
            self.__set_duo()
        elif self.cli_type == eCLI_TYPE.TYPE_TDUO :
            self.__set_duo()
        elif self.cli_type == eCLI_TYPE.TYPE_INSTALL :
            self.__set_install()
    
    def __param_test(self, p, target, val) :
        for i in Properties().get_param_using(self.cli_type) :
            if target == i :
                p.param[i] = self.param[i][val]
            else :
                p.param[i] = self.param[i][0]
            p.value[i] = self.value[i][0]
    
    def __value_test(self, p, target, val) :
        for i in Properties().get_param_using(self.cli_type) :
            if target == i :
                p.value[i] = self.value[i][val]
            else :
                p.value[i] = self.value[i][0]
            p.param[i] = self.param[i][0]
    
    def __line_test(self, p, target) :
        for i in Properties().get_param_using(self.cli_type) :
            if target == i :
                p.param[i] = ""
                p.value[i] = ""
            else :
                p.param[i] = self.param[i][0]
                p.value[i] = self.value[i][0]
        
    def run(self, args=list()):
        self.auto_test_run()

    def help(self, args):
        hel = u'''autocli.py [command]
        [command]
        필수 옵션
            -t : 어떤 사이트를 테스트할지 설정합니다
                fx(staging fx), tfx(test fx), gx(staging gx), tgx(test gx), alt(staging alt), talt(test alt), duo(staging duo), tduo(test duo), install(설치형)
        '''
        print (hel)
        sys.exit()
    
    def __utf_8_convert(self, lis) :
        out = list()
        for line in lis :
            out.append(line.decode("UTF-8", "replace").strip())
        return "\n".join(out)
    
    def run_command(self, command, cmd) :
        time.sleep(1.2)
        out_line = None
        if cmd == 0 :
            f = open(current_path + os.sep + "config.properties", "w")
            f.write(command)
            f.close()
            f = open(current_path + os.sep + "config.properties", "r")
            #print(f.read())
            f.close()
            cmd_line = "java -jar %sxShieldCLI.jar" %(current_path + os.sep )
            print("\n\n")
            print(cmd_line)
            print(command)
            print("\n\n")
            out_line = runprocess.RunProcessOut(cmd_line)
            log_str = "[CMD]\n%s\n\n[PARAM]\n%s\n\n[RESULT]\n%s\n\n" % (cmd_line, command, self.__utf_8_convert(out_line))
            print(log_str)
            self.log_list.append(log_str)
            print("LOG LIST LEN ::: %d" %len(self.log_list))
        else :
            if os.path.isfile(current_path + os.sep + "config.properties") == True :
                os.remove(current_path + os.sep + "config.properties")
            cmd_line = "java -jar %sxShieldCLI.jar %s" %(current_path + os.sep, command)
            print("\n\n")
            print(cmd_line)
            print("\n\n")
            out_line = runprocess.RunProcessOut(cmd_line)
            log_str = "[CMD]\n%s\n\n[PARAM]\n%s\n\n[RESULT]\n%s\n\n" % (cmd_line, "", self.__utf_8_convert(out_line))
            print(log_str)
            self.log_list.append(log_str)
            print("LOG LIST LEN ::: %d" %len(self.log_list))

    def auto_test_run(self) :
        for cmd in range(1 + 1) :
        #CMD만 테스트할땐 밑에꺼만 풀어요
        #for cmd in range(1, 2) :
            #Normal Test
        #################################################################################
            with ThreadPoolExecutor(max_workers=1) as exe:
                p = Properties()
                p.cli_type = self.cli_type
                self.__param_test(p, ePARAM_TYPE.TYPE_END, 0)
                config_prop = p.get_config_properties(p, cmd)
                exe.submit(self.run_command, config_prop, cmd)
                exe.shutdown(wait=True)
                print("\n")
                print("*" * 30)
        #################################################################################

            #Param Test
            param_count = 0
            for i in Properties().get_param_using(self.cli_type) :
                param_count = param_count - 1
                for k in range(len(self.param[i])) :
                    param_count = param_count + 1
        #################################################################################
            with ThreadPoolExecutor(max_workers=param_count + 1) as exe:
                count = 0
                for i in Properties().get_param_using(self.cli_type) :
                    for k in range(len(self.param[i])) :
                        if k == 0 : continue
                        p = Properties()
                        p.cli_type = self.cli_type
                        self.__param_test(p, i, k)
                        config_prop = p.get_config_properties(p, cmd)
                        count = count + 1
                        print("PARAM TEST -- MAX COUNT [%d]    CURRENT COUNT [%d]" %(param_count, count))
                        exe.submit(self.run_command, config_prop, cmd)
                        time.sleep(0.5)
                        print("\n")
                exe.shutdown(wait=True)
                print("*" * 30)
        #################################################################################

            #Value Test
            value_count = 0
            for i in Properties().get_param_using(self.cli_type) :
                value_count = value_count - 1
                for k in range(len(self.value[i])) :
                    value_count = value_count + 1
        #################################################################################
            with ThreadPoolExecutor(max_workers=value_count + 1) as exe:
                count = 0
                for i in Properties().get_param_using(self.cli_type) :
                    for k in range(len(self.value[i])) :
                        if k == 0 : continue
                        p = Properties()
                        p.cli_type = self.cli_type
                        self.__value_test(p, i, k)
                        config_prop = p.get_config_properties(p, cmd)
                        count = count + 1
                        print("VALUE TEST -- MAX COUNT [%d]    CURRENT COUNT [%d]" %(value_count, count))
                        exe.submit(self.run_command, config_prop, cmd)
                        time.sleep(0.5)
                        print("\n")
                exe.shutdown(wait=True)
                print("*" * 30)
        #################################################################################

            #line Test
            line_count = 0
            for i in Properties().get_param_using(self.cli_type) :
                line_count = value_count + 1
        #################################################################################
            with ThreadPoolExecutor(max_workers=line_count + 1) as exe:
                count = 0
                for i in Properties().get_param_using(self.cli_type) :
                    p = Properties()
                    p.cli_type = self.cli_type
                    self.__line_test(p, i)
                    config_prop = p.get_config_properties(p, cmd)
                    count = count + 1
                    print("LINE TEST -- MAX COUNT [%d]    CURRENT COUNT [%d]" %(line_count, count))
                    exe.submit(self.run_command, config_prop, cmd)
                    time.sleep(0.5)
                    print("\n")
                exe.shutdown(wait=True)
                print("*" * 30)
        #################################################################################
        #리스트가 제대로 안담겨서 ThreadPoolExecutor를 사용
        print("LOG LIST LEN ::: %d" %len(self.log_list))
        f = open(current_path + os.sep + "log.txt", "w")
        f.write(("*" * 100 + "\n\n").join(self.log_list))
        f.close()

        


class Properties :
    def __init__(self, *args, **kwargs):
        self.param = dict()
        self.param.setdefault(ePARAM_TYPE.TYPE_USERID, "userId")
        self.param.setdefault(ePARAM_TYPE.TYPE_PASSWD, "passwd")
        self.param.setdefault(ePARAM_TYPE.TYPE_ENGINE_VER, "engineVer")
        self.param.setdefault(ePARAM_TYPE.TYPE_PACK_OPT, "packOpt")
        self.param.setdefault(ePARAM_TYPE.TYPE_DETECT_OPT, "detectOpt")
        self.param.setdefault(ePARAM_TYPE.TYPE_ORGN_PATH, "orgnPath")
        self.param.setdefault(ePARAM_TYPE.TYPE_SAVE_PATH, "savePath")
        self.param.setdefault(ePARAM_TYPE.TYPE_CONFIRM, "confirm")
        self.param.setdefault(ePARAM_TYPE.TYPE_OVER_WRITE, "overwrite")
        self.param.setdefault(ePARAM_TYPE.TYPE_ENC_PATH, "encPath")
        self.param.setdefault(ePARAM_TYPE.TYPE_SAVE_ORGN_APK, "saveOrgnApk")
        self.param.setdefault(ePARAM_TYPE.TYPE_SITE, "site")
        self.param.setdefault(ePARAM_TYPE.TYPE_DPG_MESSAGE, "dpgMessage")
        self.param.setdefault(ePARAM_TYPE.TYPE_DPG_RNOTE, "dpgRNote")
        self.param.setdefault(ePARAM_TYPE.TYPE_KEYSTORE, "keystore")
        self.param.setdefault(ePARAM_TYPE.TYPE_STOREPASS, "storepass")
        self.param.setdefault(ePARAM_TYPE.TYPE_ALIAS, "alias")
        self.param.setdefault(ePARAM_TYPE.TYPE_KEYPASS, "keypass")
        
        self.value = dict()
        self.value.setdefault(ePARAM_TYPE.TYPE_USERID, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_PASSWD, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_ENGINE_VER, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_PACK_OPT, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_DETECT_OPT, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_ORGN_PATH, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_SAVE_PATH, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_CONFIRM, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_OVER_WRITE, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_ENC_PATH, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_SAVE_ORGN_APK, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_SITE, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_DPG_MESSAGE, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_DPG_RNOTE, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_KEYSTORE, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_STOREPASS, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_ALIAS, "")
        self.value.setdefault(ePARAM_TYPE.TYPE_KEYPASS, "")
        self.cli_type = ""

    def get_config_properties(self, p, cmd) :
        config_properties = list()
        chain = "="
        line = "\n\n"
        if cmd == 1 :
            chain = " "
            line = " "
        
        for i in self.get_param_using(p.cli_type) :
            if p.param[i] == "" and p.value[i] == "" :
                continue
            if ePARAM_TYPE.TYPE_ORGN_PATH == i or \
            ePARAM_TYPE.TYPE_SAVE_PATH == i or \
            ePARAM_TYPE.TYPE_ENC_PATH == i or \
            ePARAM_TYPE.TYPE_KEYSTORE == i :
                p.value[i] = p.value[i].replace("\\", "/")

            config_properties.append(p.param[i] + chain + p.value[i])
        config_properties = line.join(config_properties)

        #print(config_properties)
        return config_properties

    def random_detect_option(self) :
        g_detect_option = ["T", "K", "N", "C", "J", "R", "Z", "X", "V", "G"]
        random_detect_option = list()
        random_count = random.randrange( 0, len(g_detect_option) )
        before_idx = list()
        for i in range(random_count) :
            while(1) :
                check = 0
                rand_idx = random.randrange( 0, len(g_detect_option) )
                for idx in before_idx :
                    if idx == rand_idx :
                        check = 1
                        break
                if check == 1 :
                    continue
                before_idx.append(rand_idx)
                random_detect_option.append(g_detect_option[rand_idx])
                break
        random_detect_option = "".join(random_detect_option)
        #print (random_detect_option)
        return random_detect_option

    def __random_pack_option_encryption(self) :
        #n = 난독화 비율
        #0% = n, 10~90 = n1~n9, 100% = none
        random_pack_option_encryption = list()
        START_ENCRYPTION = 0
        NOT_ENCRYPTION = 0
        ALL_ENCRYPTION = 1
        DIVISION_ENCRYPTION = 2
        END_ENCRYPTION = 3
        rand_select_encryption_Type = random.randrange(START_ENCRYPTION, END_ENCRYPTION)
        if rand_select_encryption_Type == NOT_ENCRYPTION :
            random_pack_option_encryption.append("n")
        elif rand_select_encryption_Type == ALL_ENCRYPTION :
            pass
        elif rand_select_encryption_Type == DIVISION_ENCRYPTION :
            random_pack_option_encryption.append( "n" + str(random.randrange(1, 9+1)) )
        if len(random_pack_option_encryption) == 0 :
            return ""
        return "".join(random_pack_option_encryption)
 
    def __random_pack_option_obfuscation(self) :
        #a = 코드 난독화
        #1, 2, 4
        return "a" + str(random.randrange(1, 7+1))

    def __random_pack_option_so(self) :
        #s = so 난독화
        if random.randrange(0, 1+1) == 0 :
            return ""
        else :
            return "s"

    def __random_pack_option_api(self) :
        #y = 범용 API 제외
        if random.randrange(0, 1+1) == 0 :
            return ""
        else :
            return "y"

    def __random_pack_option_process(self) :
        #m = 프로세스 이름 랜덤 변경
        if random.randrange(0, 1+1) == 0 :
            return ""
        else :
            return "m"

    def random_pack_option(self) :
        random_pack_option = list()
        random_pack_option.append(self.__random_pack_option_encryption())
        random_pack_option.append(self.__random_pack_option_obfuscation())
        random_pack_option.append(self.__random_pack_option_so())
        random_pack_option.append(self.__random_pack_option_api())
        random_pack_option.append(self.__random_pack_option_process())
        random_pack_option = "".join(random_pack_option)
        #print (random_pack_option)
        return random_pack_option

    def get_param_using(self, cli_type) :
        using = list()
        using.append(ePARAM_TYPE.TYPE_USERID)
        #if cli_type != eCLI_TYPE.TYPE_INSTALL :
            #using.append(ePARAM_TYPE.TYPE_PASSWD)
        using.append(ePARAM_TYPE.TYPE_PASSWD)
        using.append(ePARAM_TYPE.TYPE_ENGINE_VER)
        using.append(ePARAM_TYPE.TYPE_PACK_OPT)
        using.append(ePARAM_TYPE.TYPE_DETECT_OPT)
        using.append(ePARAM_TYPE.TYPE_ORGN_PATH)
        using.append(ePARAM_TYPE.TYPE_SAVE_PATH)
        using.append(ePARAM_TYPE.TYPE_CONFIRM)
        using.append(ePARAM_TYPE.TYPE_OVER_WRITE)
        using.append(ePARAM_TYPE.TYPE_ENC_PATH)
        if cli_type != eCLI_TYPE.TYPE_INSTALL :
            using.append(ePARAM_TYPE.TYPE_SAVE_ORGN_APK)
        using.append(ePARAM_TYPE.TYPE_SITE)

        if cli_type == eCLI_TYPE.TYPE_ALT or \
        cli_type == eCLI_TYPE.TYPE_TALT :
            using.append(ePARAM_TYPE.TYPE_DPG_MESSAGE)
            using.append(ePARAM_TYPE.TYPE_DPG_RNOTE)

        if cli_type == eCLI_TYPE.TYPE_INSTALL :
            using.append(ePARAM_TYPE.TYPE_KEYSTORE)
            using.append(ePARAM_TYPE.TYPE_STOREPASS)
            using.append(ePARAM_TYPE.TYPE_ALIAS)
            using.append(ePARAM_TYPE.TYPE_KEYPASS)
        return using


if __name__ == "__main__":
    ac = AutoCli()

    opt = option.option()
    #def addOpt(self, opt, argCount, bVarArg, func):
    opt.addOpt(opt="-h", argCount=0, bVarArg=True, bHelp=True, func=ac.help)
    opt.addOpt(opt="-t", argCount=1, bVarArg=True, bHelp=False, func=ac.set_test_type) #필수 옵션
    opt.addOpt(opt="default", argCount=0, bVarArg=True, bHelp=False, func=ac.run)
    opt.parsing()
    #opt.parsing(["-t", "install"])
    #opt.tprint()
    opt.run()


    pass







